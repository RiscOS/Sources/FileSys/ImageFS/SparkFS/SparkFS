/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.arcs */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>



#include "kernel.h"
#include "swis.h"

#include "Interface/SparkFS.h"

#include "Consts.h"
#include "veneers.h"
#include "Interface.h"
#include "fsentries.h"
#include "ModuleWrap.h"

#include "flex.h"
#include "fs.h"
#include "sfs.h"
#include "fx.h"
#include "scrap.h"
#include "cat.h"
#include "link.h"
#include "pw.h"
#include "callback.h"
#include "upcall.h"
#include "sfserrs.h"
#include "arcs.h"


/*****************************************************************************/

archive * arc;
int       maxarc;

static void initbuffer(int i)
{
 memset(&arc[i],0,sizeof(archive));
}


_kernel_oserror * arcsinit(void)
{
 int i;

 MAXARC=MAXARCINIT;

 if(!flex_alloc((flex_ptr)&arc,MAXARC*sizeof(archive)))
                                          return(geterror(mb_malloc_failed));
 else
 {
  for(i=0;i<MAXARC;i++) initbuffer(i);
 }

 return(NULL);
}


_kernel_oserror * setbuffers(char * args[])
{
 _kernel_oserror  * err;
 int                n;
 int                i;

 err=NULL;

 if(sscanf(args[0],"%d",&n)==1)
 {
  if(n>MAXARC)
  {
   if(!flex_extend((flex_ptr)&arc,n*sizeof(archive)))
                                          return(geterror(mb_malloc_failed));
   else
   {
    for(i=MAXARC;i<n;i++) initbuffer(i);
    MAXARC=n;
   }
  }
 }
 return(err);
}



/* scan arc structure for archive name */

_kernel_oserror * findarc(char * path,int * arcn)
{
 int i;

 for(i=0;i<MAXARC;i++)
 {
  if(arc[i].inuse)
  {
   if(!cstrcmp(arc[i].name,path))
   {
    *arcn=i;
    return(NULL);
   }
  }
 }

 *arcn=-1;
 return(NULL);
}



_kernel_oserror * newhandle(int * acn)
{
 _kernel_oserror * err;
 int               i;
 int               j;
 static int        rot;

 err=NULL;

 for(i=0;i<MAXARC;i++)
   if(!arc[i].inuse && !arc[i].files) break;

 j=0;
 if(i==MAXARC)
 {
  for(i=rot,j=0;j<MAXARC;j++,i++)
  {
   if(i>=MAXARC) i=0;

   if(!arc[i].files)
   {
    rot=i+1;
    break;
   }
  }
 }

 if(j==MAXARC) // was i 3/7/2020
 {
  err=geterror(mb_sfserr_TooManyArc);
 }
 else
 {
  *acn=i;
  if(arc[i].inuse && arc[i].image)
  {
   closeimagehandle(i);
  }
 }

 return(err);
}




/* function returns number of archive or -1 if it does not exist */
/* if not in cache, load directory                               */

static _kernel_oserror * gethandlesub(char * path,int * arcn,int fsfh)
{
 _kernel_oserror * err;
 int               good;

 while(1)
 {
  err=findarc(path,arcn);
  if(err || (*arcn)<0)
  {
   deb("findarc failed");
   if(!err)
   {
    if((*arcn)<0)
    {
     err=loadarchive(path,fsfh);
     if(!err)
     {
      err=findarc(path,arcn);
      if(!err && ((*arcn)>=0)) checkpassword(*arcn);
     }
    }
   }
   break;
  }
  else
  if(arc[*arcn].image) break;
  else
  {
   err=validate(&arc[*arcn],&good);
   if(err || !good)
   {
    deb("validate inuse=0");
    scrapcat(*arcn);
   }
   if(err || good) break;
  }
 }

 return(err);
}




_kernel_oserror * gethandle(char * path,int * arcn)
{
 _kernel_oserror * err;

 err=gethandlesub(path,arcn,0);
/* if(*arcn>=0) arc[*arcn].image=0; think this is not needed */
 return(err);
}


_kernel_oserror * imgethandle(char * path,int fsfh,int * arcn)
{
 _kernel_oserror * err;

 err=gethandlesub(path,arcn,fsfh);
 if(*arcn>=0)
 {
  arc[*arcn].image=1;
  arc[*arcn].docache=docache;
  callbackusefile(*arcn);
  arc[*arcn].fh=fsfh;
 }
 return(err);
}




_kernel_oserror * transpath(char * src,char * dest)
{
 _kernel_swi_regs rx;
 _kernel_oserror * err;

 rx.r[0]=(int)src;
 rx.r[1]=(int)dest;
 rx.r[2]=MAXPATH;

 err=_kernel_swi(OS_GSTrans,&rx,&rx);

 return(err);
}




static void hashname(char * name)
{
 while(*name)
 {
  if(*name==':') *name='#';
  name++;
 }
}



/* *SparkFSFiler_Opendir <filename> */

_kernel_oserror * openfiler(char * args[])
{
 _kernel_oserror * err;
 char              arcname[MAXPATH];
 char              arcpath[MAXPATH];

 err=transpath(args[0],arcname);

 if(!err)
 {
  hashname(arcname);
  sprintf(arcpath,"Filer_OpenDir SparkFS#%s:$",arcname);

  err=oscli(arcpath);
 }

 return(err);
}




/* Create <type> <name>   */

_kernel_oserror * createarchive(char * args[])
{
 _kernel_oserror * err;
 int               type;
 int               acn;
 int               code;
 char              path[MAXPATH];
 char              temp[MAXPATH];


 type=atoi(args[0]);

          err=transpath(args[1],temp);
 if(!err) err=fs_canonicalpath(temp,path,sizeof(path),NULL,NULL);

 err=newhandle(&acn);
 if(!err)
 {
  code=fexists(path);
  if(code!=0) err=geterror(mb_sfserr_ArcExists);
  else
  {
   err=initcat(acn,path);
   if(!err)
   {
    arc[acn].type=type;
    err=createarchivelo(acn,type);
    if(err) scrapcat(acn);
   }
  }
 }

 return(err);
}



 /* Method <type> <method> */

_kernel_oserror * setmethod(char * args[])
{
 _kernel_oserror * err;
 int               type;
 int               method;

 type=atoi(args[0]);
 method=atoi(args[1]);

 err=setmethodlo(type,method);

 return(err);
}




/* Convert <source> <dest> <dest type> */

_kernel_oserror * convert(char * args[])
{
 _kernel_oserror * err;
 int    acnsrc;
 int    acndest;
 char   src[MAXPATH];
 char   dest[MAXPATH];
 char   temp[MAXPATH];
 int    type;
 int    code;


 type=atoi(args[2]);

          err=transpath(args[0],temp);
 if(!err) err=fs_canonicalpath(temp,src,sizeof(src),NULL,NULL);
 if(!err) err=transpath(args[1],temp);
 if(!err) err=fs_canonicalpath(temp,dest,sizeof(dest),NULL,NULL);

 if(!err) err=gethandle(src,&acnsrc);
 if(!err)
 {
  code=fexists(dest);
  if(code!=0) err=geterror(mb_sfserr_ArcExists);
  else
  {
   err=newhandle(&acndest);
   if(!err)
   {
    err=diropenall(acnsrc);
    if(!err) err=copycat(&arc[acnsrc],&arc[acndest]);

    if(!err)
    {
     arc[acndest].type=type;
     strcpy(arc[acndest].name,dest);


   /*  printf("dest=%d src=%d %s %s\n",arc[acndest].type,arc[acnsrc].type,
                   arc[acnsrc].name,arc[acndest].name); */


     calcdirlens(&arc[acndest]);

     err=convertlo(acnsrc,acndest);

     if(err) scrapcat(acndest);
    }
   }
  }
 }

 return(err);
}



_kernel_oserror * mountarchive(char * args[])
{
 _kernel_oserror * err;
 int               acn;
 char              name[MAXPATH];
 char              temp[MAXPATH];

          err=transpath(args[0],temp);
 if(!err) err=fs_canonicalpath(temp,name,sizeof(name),NULL,NULL);
 if(!err) err=gethandle(name,&acn);

 return(err);
}


_kernel_oserror * dismountarchive(char * args[])
{
 _kernel_oserror * err;
 int               acn;
 char              name[MAXPATH];
 char              temp[MAXPATH];


 if(!cstrcmp(args[0],"-all"))
 {
  err=NULL;

  for(acn=0;acn<MAXARC;acn++)
  {
   if(arc[acn].inuse)
   {
    closeallfiles(acn);
    closeimagehandle(acn);
    closefiler(acn,CFIM|CFSC);
   }
  }
 }
 else
 {
           err=transpath(args[0],temp);
  if(!err) err=fs_canonicalpath(temp,name,sizeof(name),NULL,NULL);
  if(!err) err=findarc(name,&acn);
  if(!err && acn>=0)
  {
   closeallfiles(acn);
   closeimagehandle(acn);
   closefiler(acn,CFIM|CFSC);
  }
 }

 return(err);
}




