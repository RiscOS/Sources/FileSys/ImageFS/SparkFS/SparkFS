/* Copyright 1992 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* Title: c.flex
 * Purpose: provide memory allocation for interactive programs requiring
 *          large chunks of store.
 * History: IDJ: 06-Feb-92: prepared for source release
 */



#define BOOL int
#define TRUE 1
#define FALSE 0


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "kernel.h"
#include "swis.h"

#include "Interface/SparkFS.h"

#include "mem.h"
#include "fs.h"
#include "sfserrs.h"
#include "flex.h"






/* #define FLEXEX */


typedef struct
{
 flex_ptr anchor;      /* *anchor should point back to here. */
 int size;             /* in bytes. Exact size of logical area. */
                       /* then the actual store follows. */

#ifdef FLEXEX

 flex_lockstr * lock;

#endif


} flex__rec;




static void flex__fail(int i)
{
/* printf("xxxxxxxxxxxxxxxxxxx i=%d xxxxxxxxxxxxxxxxxxxxx",i); */

 memcorrupt(i);
}



static int roundup(int i)
{
  return 0xfffffffc & (i + 3);
}


static char *flex__freep;       /* free flex memory */
static char *flex__lim;         /* limit of flex memory */
static char *flex__base;


/* From base upwards, it's divided into store blocks of
  a flex__rec
  the space
  align up to next word.
*/




static BOOL flex__more(int n)
{
 return(memextend(n));
}



static void flex__give(void)
{
 memcontract();
}



static BOOL flex__ensure(int n)
{
 n-=flex__lim-flex__freep;
 if(n<=0 || flex__more(n)) return(TRUE);
 else                      return(FALSE);
}



BOOL flex_alloc(flex_ptr anchor, int n)
{
 flex__rec *p;

/*  printf("alloc %x %d",anchor,n);  */

 if(n<0 || ! flex__ensure(sizeof(flex__rec) + roundup(n)))
 {
  *anchor = 0;
  return FALSE;
 }

 p = (flex__rec*) flex__freep;
 flex__freep += sizeof(flex__rec) + roundup(n);

 p->anchor = anchor;
 p->size = n;

#ifdef FLEXEX
   p->lock=NULL;
#endif

 *anchor = p + 1; /* sizeof(flex__rec), that is */
 return TRUE;
}




static void flex__reanchor(flex__rec *p, int by)
{
#ifdef FLEXEX
 flex_lockstr * lock;

#endif



 /* Move all the anchors from p upwards. This is in anticipation
  of that block of the heap being shifted. */

 while (1)
 {
  if((int) p>=(int) flex__freep) break;
  if(*(p->anchor) != p + 1) flex__fail(6);


#ifdef FLEXEX
  if((lock=p->lock)!=NULL)
  {
   while(lock)
   {
    deb("lock data");

    if(lock->lockfn) lock->lockfn(lock,lock->data,by);
    else
    {
     *(lock->data)=(((char*)(*(lock->data)))+by);
    }

    lock=lock->next;
   }
  }
#endif


  *(p->anchor) = ((char*) (p + 1)) + by;
  p = (flex__rec*) (((char*) (p + 1)) + roundup(p->size));
 }
}


void flex_free(flex_ptr anchor)
{
 flex__rec *p = ((flex__rec*) *anchor) - 1;
 int roundsize = roundup(p->size);
 flex__rec *next = (flex__rec*) (((char*) (p + 1)) + roundsize);


 if(p->anchor!=anchor)
 {
  flex__fail(0);
 }

 flex__reanchor(next, - (sizeof(flex__rec) + roundsize));

 memmove(p,next,flex__freep-(char*) next);

 flex__freep-=sizeof(flex__rec) + roundsize;

 flex__give();

 *anchor = 0;
}






#ifdef FLEXEX


void flex_addlock(flex_lockstr * lock,flexlockfn flex,flex_ptr ptr)
{
 flex__rec    * p;
 flex__rec    * q;
 flex_lockstr * next;
 void         * data;

 p=(flex__rec *)flex__base;
 data=*ptr;

 memset(lock,0,sizeof(flex_lockstr));

 if(data)
 {
  while(((int)p)<((int)flex__freep))
  {
   q=(flex__rec*)(((char*)(p + 1))+roundup(p->size));

   if(data>=(void*)p && data<(void*)q)
   {
    next=p->lock;
    p->lock=lock;

    if(next) next->prev=lock;
    lock->next=next;
    lock->prev=NULL;
    lock->lockfn=flex;
    lock->data=ptr;

    deb("add lock");

    break;
   }

   p=q;
  }
 }
}


void flex_remlock(flex_lockstr * lock)
{
 flex__rec    * p;
 flex_lockstr * next;
 flex_lockstr * prev;

 p=(flex__rec *)flex__base;

 if(lock->data)
 {
  while(((int)p)<((int)flex__freep))
  {
   if((next=p->lock)!=NULL)
   {
    while(next)
    {
     if(next==lock)
     {
      prev=next->prev;
      next=next->next;
      if(next) next->prev=prev;
      if(prev) prev->next=next;
      else     p->lock=next;

      deb("rem lock");

      break;
     }
     next=next->next;
    }
   }

   p=(flex__rec*)(((char*)(p + 1))+roundup(p->size));
  }
 }
}

#endif





int flex_size(flex_ptr anchor)
{
 flex__rec *p = ((flex__rec*) *anchor) - 1;
 if(p->anchor!=anchor)
 {
  flex__fail(4);
 }
 return(p->size);
}



int flex_extend(flex_ptr anchor, int newsize)
{
 flex__rec *p = ((flex__rec*) *anchor) - 1;

 /* printf("flex_extend %x %d",anchor,newsize);  */

 return(flex_midextend(anchor, p->size, newsize - p->size));
}



BOOL flex_midextend(flex_ptr anchor, int at, int by)
{
 flex__rec *p;
 flex__rec *next;


 p=((flex__rec*) *anchor) - 1;

 if(p->anchor!=anchor)
 {
  flex__fail(1);
 }

 if(at>p->size)
 {
  flex__fail(2);
 }

 if(by < 0 && (-by) > at)
 {
  flex__fail(3);
 }

 if(by == 0)
 {
  /* do nothing */
 }
 else
 if(by > 0)
 { /* extend */

  int growth = roundup(p->size + by) - roundup(p->size);
  /* Amount by which the block will actually grow. */

  if(!flex__ensure(growth))
  {
   return FALSE;
  }

  next = (flex__rec*) (((char*) (p + 1)) + roundup(p->size));

  /* The move has to happen in two parts because the moving
    of objects above is word-aligned, while the extension within
    the object may not be. */

  flex__reanchor(next, growth);

  memmove(((char*) next) + roundup(growth),next,flex__freep - (char*) next);

  flex__freep += growth;

  memmove(((char*) (p + 1)) + at + by,((char*) (p + 1)) + at,p->size - at);
  p->size += by;

 }
 else
 { /* The block shrinks. */
  int shrinkage;

  next = (flex__rec*) (((char*) (p + 1)) + roundup(p->size));

  by = -by; /* a positive value now */
  shrinkage = roundup(p->size) - roundup(p->size - by);
  /* a positive value */

  memmove(((char*) (p + 1)) + at - by,((char*) (p + 1)) + at,p->size - at);
  p->size -= by;

  flex__reanchor(next, - shrinkage);

  memmove(((char*) next) - shrinkage,next,flex__freep - (char*) next);

  flex__freep -= shrinkage;

  flex__give();

 };
 return(TRUE);
}




/* called when we propose to change amount of stuff in flex block */

int flex_chunk(flex_ptr anchor,int size,int chunksize)
{
 flex__rec *p = ((flex__rec*) *anchor)-1;

 if((size>=p->size) || (p->size>(size+(3*chunksize)/2)))
 {
  while(chunksize)
  {
   if(flex_extend(anchor,(size/chunksize+1)*chunksize)) return(TRUE);
   chunksize/=2;
  }
  return(FALSE);
 }

 return(TRUE);
}



_kernel_oserror * flex_chunke(flex_ptr anchor,int size,int chunksize)
{
 if(!flex_chunk(anchor,size,chunksize)) return(geterror(mb_malloc_failed));
 else                                   return(NULL);
}


_kernel_oserror * flex_alloce(flex_ptr anchor, int n)
{
 if(!flex_alloc(anchor,n)) return(geterror(mb_malloc_failed));
 else                      return(NULL);
}




int flex_init(char * chunk,int size)
{
 flex__freep=chunk;
 flex__lim=chunk+size;
 flex__base=chunk;
 return(1);
}


int flex_newmax(char * chunk,int size)
{
 flex__lim=chunk+size;
 return(1);
}


int flex_inuse(void)
{
 return(flex__freep-flex__base);
}


int flex_max(void)
{
 return(flex__lim-flex__base);
}


int flex_reloc(char * newbase)
{
 int         shift;
 flex__rec * p;
 char      * anchor;
 char      * oldbase;
 char      * oldfreep;

 shift=newbase-flex__base;

 oldbase=flex__base;
 oldfreep=flex__freep;

 flex__base+=shift;
 flex__freep+=shift;
 flex__lim+=shift;

 p=(flex__rec *)flex__base;

 while(((int)p)<((int)flex__freep))
 {
/*printf("shift=%d *(p->anchor)=%x p+1=%x",shift,*(p->anchor),(char*)(p+1));*/

  anchor=(char*)(p->anchor);

  if(anchor>=oldbase && anchor<oldfreep) p->anchor=(flex_ptr)(anchor+shift);

  if(*(p->anchor)!=(((char*)(p+1))-shift))
  {
   flex__fail(6);
   return(0);
  }
  *(p->anchor)=((char*)(p+1));

  p=(flex__rec*)(((char*)(p + 1))+roundup(p->size));
 }

 return(1);
}



/* return 1 if corrupt */


int flex_check(void)
{
 flex__rec * p;
 char      * q;

 p=(flex__rec *)flex__base;

 while(((int)p)<((int)flex__freep))
 {
  q=*(p->anchor);

  if(q<((char*)flex__base) || q>((char*)flex__freep))
  {
   deb("flex pointer at %x points to illegal address.",(int)p);
   return(1);
  }

  if(*(p->anchor)!=(((char*)(p+1))))
  {
   deb("bad flex blockat %x.",(int)p);
   return(1);
  }

/*  deb("block %x len=%d.",p,p->size);  */

  p=(flex__rec*)(((char*)(p + 1))+roundup(p->size));
 }

 return(0);
}

