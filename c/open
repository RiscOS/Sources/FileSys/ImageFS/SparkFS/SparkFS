/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.open */


#include <stdio.h>
#include <stdlib.h>


#include "kernel.h"
#include "swis.h"

#include "Interface/SparkFS.h"

#include "Consts.h"
#include "veneers.h"
#include "Interface.h"
#include "fsentries.h"
#include "ModuleWrap.h"

#include "fs.h"
#include "mem.h"
#include "sfs.h"
#include "arcs.h"
#include "image.h"
#include "sff.h"
#include "sfi.h"
#include "sfserrs.h"



static _kernel_oserror *  formpath(FSEntry_Open_Parameter * parm,
                                               char ** path,char ** name)
{
 return(makepath(parm->open_definition.filename,
                   parm->open_definition.special.special_field,path,name));
}



static _kernel_oserror *  imformpath(FSEntry_Open_Parameter * parm,int fsfh,
                                               char ** path,char ** name)
{
 return(immakepath(parm->open_definition.filename,fsfh,path,name));
}



_kernel_oserror *fsentry_open(FSEntry_Open_Parameter *parm)
{
 _kernel_oserror *err;
 int              acn;
 char           * arcpath;
 char           * arcname;


 deb("open reason=%d ",parm->open_definition.reason);

 err=memcheck();
 if(!err)
 {
  err=formpath(parm,&arcpath,&arcname);
  if(!err)
  {
   deb("open switch");

   err=gethandle(arcname,&acn);
   if(!err) err=truncatepath(arcpath,acn,fstruncate);
   if(!err)
   {
    switch(parm->open_definition.reason)
    {
     case       FSEntry_Open_Reason_Update:
                                        err=OpenUpdate(acn,arcpath,parm);
                                        break;

     case     FSEntry_Open_Reason_OpenRead:
                                        err=OpenRead(acn,arcpath,parm);
                                        break;

     case FSEntry_Open_Reason_CreateUpdate:
                                        err=OpenCreateUpdate(acn,arcpath,parm);
                                        break;

                                default:
                                        err=geterror(mb_nfserr_BadParameters);
                                        break;
    }
   }
   clearpath(arcpath,arcname);
  }
 }

 deb("open x");

 return(err);
}



_kernel_oserror *imentry_open(FSEntry_Open_Parameter *parm)
{
 _kernel_oserror *err;
 int              acn;
 int              fsfh;
 char           * arcpath;
 char           * arcname;



 deb("im open reason=%d ",parm->open_definition.reason);

 err=memcheck();
 if(!err)
 {
  err=imagegetfsfh(parm->open_definition.special.imfh,&fsfh);
  if(!err)
  {
   err=imformpath(parm,fsfh,&arcpath,&arcname);
   if(!err)
   {
    err=imgethandle(arcname,fsfh,&acn);
    if(!err) err=truncatepath(arcpath,acn,fstruncate);

    if(!err)
    {
     switch(parm->open_definition.reason)
     {
      case       FSEntry_Open_Reason_Update:
                                         err=OpenUpdate(acn,arcpath,parm);
                                         break;

      case     FSEntry_Open_Reason_OpenRead:
                                         err=OpenRead(acn,arcpath,parm);
                                         break;

      case FSEntry_Open_Reason_CreateUpdate:
                                        err=OpenCreateUpdate(acn,arcpath,parm);
                                        break;

                                 default:
                                         err=geterror(mb_nfserr_BadParameters);
                                         break;
     }
    }
    clearpath(arcpath,arcname);
   }
  }
 }

 deb("im open x");

 return(err);
}

