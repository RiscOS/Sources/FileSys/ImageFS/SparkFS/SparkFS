/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */

#define FS_SWI 0x445C0

#define LINKSWI (FS_SWI+0)

#define ADDLINK 0
#define REMLINK 1


#define MEMSWI  (FS_SWI+1)

#define FLEXALLOC     0
#define FLEXEXTEND    1
#define FLEXFREE      2
#define FLEXSET       3
#define FLEXMIDEXTEND 4
#define FLEXCHECK     5
#define FLEXCHUNK     6



#define INFOSWI (FS_SWI+2)


#define CODESWI (FS_SWI+3)

#define CODECRC16    0
#define CODECRC32    1

#define CODEGARBLE   1
#define CODEDES      2

#define CODEOPEN     2
#define CODEENCRYPT  3
#define CODEDECRYPT  4
#define CODECLOSE    5


#define UTILSWI (FS_SWI+4)

#define UTILPARENT     0
#define UTILSETNO      1
#define UTILINS        2
#define UTILREM        3
#define UTILTYPE       4
#define UTILLENS       5
#define UTILOPENSCRAP  6
#define UTILCLOSESCRAP 7
#define UTILERROR      8



#define USERSWI (FS_SWI+5)

#define USERIDENT 0
#define USERCAT   1




#define STRINGSWI (FS_SWI+6)

#define STRINGPTR     0
#define STRINGCREATE  1
#define STRINGRM      2
#define STRINGASSIGN  3
#define STRINGCONTROL 4


#define SPARK1 1
#define SPARK2 2
#define PKARC  3
#define ZIP    4
#define ZOO    5
#define LZH    6
#define TAR    7
#define ARCFS  8
#define ARJ    9


#define PIT      10
#define SIT      11
#define SITD     12
#define SITSX    13
#define COMPAC   14
#define COMPACSX 15

#define PACKDTYPE 16

#define CAB       17
#define CPIO      18

#define RAR       19



#define MAXPATH 256

#define MAXNLEN 256

#define PWLEN   32

#define DNFILE  -1
#define DFLAT   -2
#define DARC    -3
#define DXFILE  -4

#define IARC  0
#define IFILE 1
#define IDISC 2


#define COMPATIBLE 100



#define READONLY   1
#define DXFBIT     2
#define PRIVATE    2
#define ANYPUBLIC -3
#define ANYLOCAL  -2
#define LISTEND   -1
#define ARCLISTHDR 12



#define SCRIPT  0x5F4
#define ARCHIVE 0xDDC
#define DATA    0xFFD
#define TEXT    0xFFF
#define TARFILE 0xC46
#define PACKD   0x68E


#define DIR     0x1000
#define APP     0x2000


/* Error allocations */
#define mb_malloc_failed              0x14200
#define mb_nfserr_BadParameters       0x14201
#define mb_nfserr_DummyFSDoesNothing  0x14202
#define mb_sfserr_NotFound            0x14203
#define mb_sfserr_TooManyArc          0x14204
#define mb_sfserr_NewArc              0x14205
#define mb_sfserr_BadArc              0x14206
#define mb_sfserr_ArcExists           0x14207
#define mb_sfserr_NeedShriek          0x14208
#define mb_sfserr_CatNotFound         0x14209
#define mb_sfserr_ReadOnlyArc         0x1420A
#define mb_sfserr_CorruptData         0x1420B
#define mb_sfserr_BadMethod           0x1420C
#define mb_sfserr_BadBits             0x1420D
#define mb_sfserr_NonMethod           0x1420E
#define mb_sfserr_BadNoBits           0x1420F
#define mb_sfserr_BadEMethod          0x14210
#define mb_sfserr_PassWordTooLong     0x14211
#define mb_sfserr_PassWordMissing     0x14212
#define mb_sfserr_NonEMethod          0x14213
#define mb_sfserr_BadScrap            0x14214
#define mb_sfserr_BadNbits            0x14215
#define mb_sfserr_NoCSD               0x14216
#define mb_sfserr_NoLib               0x14217
#define mb_sfserr_WriteErr            0x14218
#define mb_sfserr_ReadErr             0x14219
#define mb_sfserr_BadUncompress       0x14220
#define mb_sfserr_BadMacType          0x14221
#define mb_sfserr_MemoryCorrupt       0x14222
#define mb_sfserr_BadHdr              0x14223
#define mb_sfserr_CendirOffset        0x14224
#define mb_sfserr_CendirFiles         0x14225
#define mb_sfserr_CendirFileOffset    0x14226
#define mb_sfserr_CendirNotFound      0x14227
#define mb_sfserr_EnddirNotFound      0x14228
#define mb_nfserr_ArchiveDoesNothing  0x14229
#define mb_nfserr_TooManyFiles        0x1422A
#define mb_nfserr_BadImage            0x1422B


#define mb_sfserr_DirNotEmpty         0x142B4
#define mb_sfserr_Access              0x142BD
#define mb_sfserr_FileOpen            0x142C2
#define mb_sfserr_Locked              0x142C3
#define mb_sfserr_AlreadyExists       0x142C4
#define mb_sfserr_TypesDontMatch      0x142C5
#define mb_sfserr_BadFileName         0x142CC
#define mb_sfserr_FileNotFound        0x142D6


typedef struct heads
{
 int                name;             /* file name */

 int                crc;

 int                size;
 int                length;          /* true file length */

 int                dirn;

 int                hdrver;
 int                hsize;

 int                load;
 int                exec;
 int                acc;
 int                fp;
 int                fshandle;      /* if file open, this points to filestr */

} heads;




typedef struct xheads
{
 char               name[MAXNLEN];   /* file name */
 heads              hdr;

} xheads;





typedef struct archive
{
 char     name[MAXPATH];
 int      lru;
 char     inuse;
 char     image;
 char     modified;
 char     mode;
 int      files;
 int      type;

 int      ro:1;
 int      docache:1;
 int      cachestate:6;
 int      spare:24;

 int      fh;
 heads *  hdr;
 int      nofiles;        /* no of files in archive        */
 int      fmain;          /* no of files in root directory */
 int      link;
 int      timelo;
 int      timehi;
 int      code;
 char     pw[PWLEN];
 int      cent;
 int      end;
 int      loc;
 int      size;
 int      mnlen;

} archive;



typedef union linkblock
{

 struct
 {
  archive * arc;
  int       fn;
  char    * path;
 } updatecat;


 struct
 {
  archive * arc;
  int       fn;
  char    * path;
 } loadcat;


 struct
 {
  archive * arc;
  int       fn;
  char    * path;
 } deletefile;


 struct
 {
  archive * arc;
  heads   * hdr;
  int       ins;
  int       ind;
  int       ow;
  char    * path;
  char    * leaf;
 } createdir;         /* need to keep this and createfile same size */


 struct
 {
  archive * arc;
  heads   * hdr;
  int       ins;
  int       ind;
  int       ow;
  char    * path;
  char    * leaf;
 } createfile;        /* need to keep this and createdir same size */


 struct
 {
  archive * arc;
  int       fn;
  char    * src;
  int       slen;
  char    * dest;
  int       dlen;
  int       dfh;
 } loadfile;

 struct
 {
  archive * arc;
  int       fn;
  int       slen;
  int       dlen;
 } openload;

 struct
 {
  archive * arc;
  int       fn;
 } closeload;




 struct
 {
  archive * arc;
  heads   * hdr;
  int       ins;
  int       ind;
  int       ow;
  int       sfh;
  char    * src;
  char    * leaf;
 } savefile;

 struct
 {
  archive * arc;
  heads   * hdr;
  int       ins;
  int       ind;
  int       ow;
  int       sfh;
  char    * src;
  char    * leaf;
 } opensave;

 struct
 {
  archive * arc;
  heads   * hdr;
  int       ins;
  int       ind;
  int       ow;
  int       sfh;
  char    * src;
  char    * leaf;
 } closesave;



 struct
 {
  archive * arc;
  int       fn1;
  int       fn2;
  int       ind;
  int       size;
  char *    name;
 } rename;


 struct
 {
  archive * arc;


 } create;


 struct
 {
  archive * arc1;
  archive * arc2;


 } convert;


 struct
 {
  char * archivetypes;
  char * compressiontypes;
  char * codetypes;
  char * conversiontypes;
  char * modulebase;
 } info;



 struct
 {
  int arctype;
  int compressiontype;

 } method;


 struct
 {
  archive * arc;
  int       ok;

 } validate;


 struct
 {
  archive * arc;
  int       dirn;
  int       open;

 } diropen;


 struct
 {
  archive * arc;

 } flush;


} linkblock;



typedef _kernel_oserror * (*linkfn)(linkblock * block);


typedef struct flink
{
 linkfn updatecat;
 linkfn loadcat;
 linkfn deletefile;
 linkfn createdir;
 linkfn createfile;
 linkfn loadfile;
 linkfn savefile;
 linkfn opensave;
 linkfn closesave;
 linkfn openload;
 linkfn closeload;
 linkfn rename;
 linkfn create;
 linkfn convert;
 linkfn info;
 linkfn method;
 linkfn validate;
 linkfn diropen;
 linkfn flush;

} flink;
