/* Copyright 1992 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Title  : flex.h
 * Purpose: provide memory allocation for interactive programs requiring
 *          large chunks of store. Such programs must respond to memory
 *          full errors.
 *
 */

#ifndef __flex_h
#define __flex_h
typedef void **flex_ptr;
int flex_alloc(flex_ptr anchor, int n);
void flex_free(flex_ptr anchor);
int flex_size(flex_ptr);
int flex_extend(flex_ptr, int newsize);
int flex_midextend(flex_ptr, int at, int by);
int flex_chunk(flex_ptr anchor,int size,int chunksize);
int flex_init(char * chunk,int size);
int flex_inuse(void);
int flex_max(void);
int flex_reloc(char * newbase);
int flex_newmax(char * chunk,int size);
int flex_check(void);


_kernel_oserror * flex_chunke(flex_ptr anchor,int size,int chunksize);
_kernel_oserror * flex_alloce(flex_ptr anchor, int n);




typedef struct flex_lockstr
{
 struct flex_lockstr * next;
 struct flex_lockstr * prev;
 void               ** data;
 void (*lockfn)(struct flex_lockstr * lock,void ** data,int by);

} flex_lockstr;


typedef void (*flexlockfn)(flex_lockstr * lock,void ** data,int by);


void flex_addlock(flex_lockstr * lock,flexlockfn flex,flex_ptr ptr);
void flex_remlock(flex_lockstr * lock);


#endif
